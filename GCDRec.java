public class GCDRec {

    public static int greatercommondivisor(int buyuksayi,int kucuksayi){
        int kalan = buyuksayi % kucuksayi;
        if (kalan == 0){
            return kucuksayi;
        }
        else{
             buyuksayi = kucuksayi;
             kucuksayi = kalan;
             return greatercommondivisor(buyuksayi,kucuksayi);
        }

    }

    public static void main(String[] args) {

        System.out.println(greatercommondivisor(Integer.parseInt(args[0]),Integer.parseInt(args[1])));
    }
}
