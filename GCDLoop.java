public class GCDLoop {
    public static void main(String[] args) {

        int buyuksayi = Integer.parseInt(args[0]);
        int kucuksayi = Integer.parseInt(args[1]);
        int kalan;

        while (true){

            kalan = buyuksayi % kucuksayi;
            buyuksayi = kucuksayi;
            kucuksayi = kalan;
            if (kalan == 0){
                break;
            }

        }
        System.out.println(buyuksayi);
    }
}
